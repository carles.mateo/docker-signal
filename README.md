# docker-signal

By Carles Mateo

Sample project to demonstrate the article in my blog:

https://blog.carlesmateo.com/2021/11/09/communicating-with-docker-containers-via-linux-signals/

It shows how to Communicate with Docker's main project, in this case a Python program, with Linux Signals.
